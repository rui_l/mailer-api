package fr.rl.resources;

import fr.rl.dto.*;
import fr.rl.services.ApiKeyService;
import fr.rl.utils.Validator;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.smallrye.common.annotation.Blocking;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@Path("/mail")
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Mailer API")
public class MailerResource {
    @Inject
    Mailer mailer;

    @RestClient
    ApiKeyService apiKeyService;

    @POST
    @Blocking
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    public Response send(@HeaderParam("apiKey") String apiKey, MailToSendDTO mailToSendDTO) {
        //check if the Api Key is not null
        if (apiKey == null)
            return Response.ok("ApiKey is required.", MediaType.TEXT_PLAIN).status(Response.Status.BAD_REQUEST).build();
        //check if the mail sender is valid
        if (mailToSendDTO == null || !isMailToSendValid(mailToSendDTO))
            return Response.ok("Please input an email sender valid.", MediaType.TEXT_PLAIN).status(Response.Status.BAD_REQUEST).build();
        //check if the apiKey is valid
        ClientDTO clientDTO = apiKeyService.getClientByApiKey(apiKey);
        if (clientDTO == null) {
            return Response.ok("ApiKey is not valid", MediaType.TEXT_PLAIN).status(Response.Status.NOT_FOUND).build();
        }
        //check if the client has an active status
        if (!clientDTO.getStatut().equalsIgnoreCase("active"))
            return Response.ok("Account is not active.", MediaType.TEXT_PLAIN).status(Response.Status.UNAUTHORIZED).build();
        if (!isMailCountValid(apiKey, clientDTO)) {
            return Response.ok("Quota reached.", MediaType.TEXT_PLAIN).status(Response.Status.UNAUTHORIZED).build();
        }
        try{
            mailer.send(Mail.withText(mailToSendDTO.getRecipient(), mailToSendDTO.getSubject(), mailToSendDTO.getContent()));
            System.out.println("Mailer sending succeeded !");
        } catch (Exception e) {
            e.printStackTrace();
            Response.ok("Mailer sending error.", MediaType.TEXT_PLAIN).status(Response.Status.SERVICE_UNAVAILABLE).build();
        }
        try {
            MailDTO mailDTO = apiKeyService.saveMail(apiKey, mailToSendDTO);
            return Response.ok(mailDTO).status(Response.Status.CREATED).build();
        } catch (Exception e) {
            return Response.ok("Database not available.", MediaType.TEXT_PLAIN).status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }

    private boolean isMailToSendValid(MailToSendDTO mailToSendDTO) {
        if (mailToSendDTO.getRecipient() == null)
            return false;
        else
            return Validator.isValidEmailAddress(mailToSendDTO.getRecipient());
    }

    private boolean isMailCountValid(String apiKey, ClientDTO clientDTO) {
        if(clientDTO.getQuotaMensuel() == 0)
            return true;
        int mailCount = apiKeyService.getMailCount(apiKey);
        return mailCount<clientDTO.getQuotaMensuel();
    }

}
