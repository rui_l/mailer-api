package fr.rl.services;

import fr.rl.dto.*;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey = "keyservice")
@Path("/apikey")
public interface ApiKeyService {
    @GET
    @Path("{apiKey}")
    ClientDTO getClientByApiKey(@PathParam("apiKey") String apiKey);

    @GET
    @Path("{apiKey}/mailCount")
    Integer getMailCount(@PathParam("apiKey") String apiKey);

    @Transactional
    @POST
    @Path("{apiKey}/mails")
    MailDTO saveMail(@PathParam("apiKey") String apiKey, MailToSendDTO mail);

}
