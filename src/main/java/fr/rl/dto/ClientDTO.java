package fr.rl.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO {
    private Integer id;
    private String nomClient;
    private String email;
    private String cle;
    private int quotaMensuel;
    private LocalDateTime dateCreation;
    private String statut;
}
