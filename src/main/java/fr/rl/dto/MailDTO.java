package fr.rl.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

public class MailDTO {
    @JsonProperty(index = 1)
    private Integer id;
    @JsonProperty(index = 2)
    private String objetMail;
    @JsonProperty(index = 3)
    private String destinataire;
    @JsonProperty(index = 4)
    private LocalDateTime dateEnvoi;
    @JsonProperty(index = 5)
    private MailDtoClient client;

    @Data
    public class MailDtoClient {
        Integer id;
        String nom;
    }
}
